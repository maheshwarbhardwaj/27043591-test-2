//Maheshwar
//27043591
//Date: 14/06/20178
//Definition of the "Van" class
class Van {
  //"default" getter and setter for the "_VanID" property
    get VanID() {
        return this._VanID                  //When the "VanID" getter "function" is used, it returns the value of the "_VanID" property 
    }

    set VanID(value) {                      //The "value" parameter has the value passed to it from the constructor
        this._VanID = value                 //Creates the "_VanID" property and sets the value passed to it from the constructor
    }
//"default" getter and setter for the "_Capacity" property
    get Capacity() {
        return this._Capacity                 //When the "Capacity" getter "function" is used, it returns the value of the "_Capacity" property 
    }
    
    set Capacity(value) {                      //The "value" parameter has the value passed to it from the constructor
        this._Capacity = value
    }
//"default" getter and setter for the "_License" property
    get License() {                            
        return this._License                   //When the "License" getter "function" is used, it returns the value of the "_License" property 
    } 

    set License(value) {                          //The "value" parameter has the value passed to it from the constructor
        this._License = value
    }
//"default" getter and setter for the "_CostPerDay" property
    get CostPerDay() {
        return this._CostPerDay                   //When the "CostPerDay" getter "function" is used, it returns the value of the "_CostPerDay" property 
    }

    set CostPerDay(value) {                       //The "value" parameter has the value passed to it from the constructor
        this._CostPerDay = value
    }
//"default" getter and setter for the "_Insurance" property
    get Insurance() {
        return this._Insurance                    //When the "Insurance" getter "function" is used, it returns the value of the "_Insurance" property   
    }

    set Insurance(value) {                           //The "value" parameter has the value passed to it from the constructor
        this._Insurance = value
    }
//Constructor for the "Van" class
constructor(VanID, Capacity, License, CostPerDay, Insurance) {
    this._VanID = VanID                                         //Pass the value of the "VanID" parameter to the "VanID" setter
    this._Capacity = Capacity                                    //Pass the value of the "Capacity" parameter to the "Capacity" setter
    this._License = License                                     //Pass the value of the "License" parameter to the "License" setter
    this._CostPerDay = CostPerDay                                 //Pass the value of the "CostPerDay" parameter to the "CostPerDay" setter
    this._Insurance = Insurance                                   //Pass the value of the "Insurance" parameter to the "Insurance" setter
}
totalCost(){
    return (this._CostPerDay + this._Insurance)          //Adding two numbers
}
}
alert("Welcome");
console.log(`\n`)

//------------
//MAIN PROGRAM
//------------
//Instantiate a "blank" new object "VanA" using the "Van" class as a "template/blueprint"
let VanA = new Van()
//Ask user for "VanA" property values (display to browser console as they are entered)
console.log(`EZ Van Hire : Van Details`)
console.log(`-------------------------`)
VanA.VanID = prompt("EZ Van Hire : Enter Van Details\n\n Van ID:")            //Uses the "VanID" setter
console.log(`Van ID : ${VanA.VanID}`)

VanA.Capacity = prompt("EZ Van Hire : Enter Van Details\n\n Load Capacity (m3)")     //Uses the "Capacity" setter
console.log(`Load Caoacity (m3) : ${VanA.Capacity}`)

VanA.License = prompt("EZ Van Hire : Enter Van Details\n\n License Type")           //Uses the "License" setter
console.log(`License type : ${VanA.License}`)

VanA.CostPerDay = prompt("EZ Van Hire : Enter Van Details\n\n Hire Cost Per Day :$")  //Uses the "CostPerDay" setter
console.log(`Hire Cost Per Day : $${VanA.CostPerDay}`)

VanA.Insurance = prompt("EZ Van Hire : Enter Van Details\n\n Insurance Per Day :$")     //Uses the "Insurance" setter
console.log(`Insurance Per Day : $${VanA.Insurance}`)

console.log(`Total Hire Cost : $${VanA.totalCost()}`)   